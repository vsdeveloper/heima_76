import Vue from 'vue'
import App from './App.vue'
import { plugin } from 'vue-function-api'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as filters from './filters/'

// 模拟的假数据
// import './mock/'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(plugin)

// 循环注册全局过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  render: h => h(App)
}).$mount('#app')
