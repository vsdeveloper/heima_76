import Mock from 'mockjs'

Mock.mock('/api/getprodlist', 'get', {
  status: 0,
  'message|2-10': [
    {
      'id|+1': 0,
      name: '@cword(2, 8)',
      ctime: new Date()
    }
  ]
})

Mock.mock('/api/addproduct', 'post', {
  status: 0,
  message: '添加成功'
})

Mock.mock(/\/api\/delproduct/, 'get', {
  status: 0,
  message: '删除品牌数据OK'
})
