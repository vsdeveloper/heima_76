import Vue from 'vue'
import App from './App.vue'
import { plugin } from 'vue-function-api'
import axios from 'axios'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as filters from './filters'

// 导入 Mock 模块
import './mock/'

Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.use(plugin)
Vue.use(ElementUI)

// 循环注册过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  render: h => h(App)
}).$mount('#app')
