import axios from 'axios'

const instance = axios.create({
  // baseURL: 'http://www.liulongbin.top:3005'
})

export const createAPI = (url, method, data) => {
  const config = {}
  if (method.toUpperCase === 'GET') {
    config.params = data
  } else {
    config.data = data
  }

  return instance({
    url,
    method,
    ...config
  })
}
