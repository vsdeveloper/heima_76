import { createAPI } from '../utils/request'

export const list = data => createAPI('/api/getprodlist', 'get', data)

export const add = data => createAPI('/api/addproduct', 'post', data)

export const remove = data =>
  createAPI(`/api/delproduct/${data.id}`, 'get', data)
